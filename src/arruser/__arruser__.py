from subprocess import run
from typing import List

__all__ = [
    'belongs_to_group',
    'get_groups',
    'get_username',
    'get_uid',
    'get_gid',
]

__GET_GROUPS_CMD_ARGS_1 = ['groups']
"""This is the first set. This is valid if the groups command exists."""

__GET_GROUPS_CMD_ARGS_2 = ['id', '-nG']
"""This does the same thing as the first cmd but uses id. Using this one for now because the other commands also
leverage ``id``.
"""


def get_gid() -> int:
    """Return the effective group ID (gid) for the logged in user.

    Returns:
        int: the effective Group ID (gid) for the current user.
    """
    r1 = run(['id', '-g'], check=True, capture_output=True)
    return int(r1.stdout.strip().decode('utf-8'))


def get_uid() -> int:
    """Return the effective User ID (uid).

    Returns:
        int: the effective User ID (uid) for the current user.
    """
    r1 = run(['id', '-u'], check=True, capture_output=True)
    return int(r1.stdout.strip().decode('utf-8'))


def get_username() -> str:
    """Return the effective username.

    Warnings:
        -   This does not request the username from the user. request_username needs to be called to do that.

    Notes:
        -   This can also be done using ``getenv('LOGNAME')`` but I figured this might be a little more consistent.

    Returns:
        str: the effective username.
    """
    r1 = run(['id', '-un'], check=True, capture_output=True)
    return r1.stdout.strip().decode('utf-8')


def get_groups() -> List[str]:
    """Get the list of all groups the currently logged in user is a member of.

    Returns:
        List[str]: the list of all groups the currently logged in user belongs too.
    """
    r1 = run(__GET_GROUPS_CMD_ARGS_2, check=True, capture_output=True)

    if r1.stdout.strip() == b'':
        raise AssertionError('Expected at least the username.')

    return r1.stdout.strip().decode('utf-8').split()


def belongs_to_group(group: str) -> bool:
    """Return whether the currently logged in user belongs to the given group or not.

    Warnings:
        -   The group name must match exactly.

    Args:
        group: The name of the group.

    Returns:
        bool: True if belongs to this group, else False.
    """
    return group in get_groups()
