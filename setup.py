from setuptools import find_packages, setup

setup(
    name='arruser',
    version='2020.6.3',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.8',
    install_requires=[],
)
