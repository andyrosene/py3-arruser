#!/bin/bash
# This is the installation script. This cannot be a python file because that would cause conflicts during install and
# uninstall.
#
# WARNING: The must be run from this directory ONLY. Any other attempt will result in an exit condition.

#
###### Ensure this is being run from a directory containing a setup.py file ######
#
if [ ! -f ./setup.py ]; then
  echo '[ERROR] this must only be run from directly within this directory.'
  exit 1
fi

#
###### Install this package ######
#
python3.8 -m pip install --upgrade --user .
if [[ "$?" != "0" ]]; then
  echo "[ERROR] Something went wrong during the installation attempt"
  exit 1
fi
